#!/bin/bash
#########################################################################################################
### Création d'un dashboard pour Kubernetes
### IUT1 Grenoble
### 27/10/2020
#########################################################################################################

BASEREP="/home/user/KUBELABS"
DASHBOARDREP=$BASEREP/labs_dashboard


#################################################
function pause(){
 echo ""
 read -s -n 1 -p "====> Presser une touche pour continuer . . ."
 echo ""
 echo ""
}

##################################################
mkdir -p $DASHBOARDREP
cd $DASHBOARDREP

echo "====> Creation du dashboard"
#wget https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0/aio/deploy/recommended.yaml
kubectl apply -f recommended.yaml
pause


echo "====> creation d'un utilisateur pour le dashboard"
cat <<-EOF >  dashboard-adminuser.yaml
  apiVersion: v1
  kind: ServiceAccount
  metadata:
    name: admin-user
    namespace: kubernetes-dashboard
EOF
kubectl apply -f dashboard-adminuser.yaml
pause

echo "====> Attribution des droits administrateurs"
cat <<-EOF >  ClusterRoleBinding.yaml
  apiVersion: rbac.authorization.k8s.io/v1
  kind: ClusterRoleBinding
  metadata:
    name: admin-user
  roleRef:
    apiGroup: rbac.authorization.k8s.io
    kind: ClusterRole
    name: cluster-admin
  subjects:
  - kind: ServiceAccount
    name: admin-user
    namespace: kubernetes-dashboard
EOF
kubectl apply -f ClusterRoleBinding.yaml
pause

#creation d'un token : resultat stocké dans token.txt
echo "Creation d'un jeton d'accès pour l'interface - ce jeton sera écrit dans token.txt"
kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}') > token.txt
pause

#lancement du proxy
echo "====> Lancement du proxy avec la commande kubectl proxy"
kubectl proxy &

#adresse du dashboard
echo "Le dashboard se trouve à cette adresse :"
echo "http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/"
pause

echo "Pour vous connecter, vous aurez besoin de spécifier le jeton suivant :"
cat token.txt

exit








