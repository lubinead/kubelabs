#!/bin/sh

USERHOME="/home/user"
TMP="/home/user/tmp"

##############################################
function pause(){
 echo ""
 read -s -n 1 -p "====> Presser une touche pour continuer . . ."
 echo ""
 echo ""
}

mkdir -p ${TMP}
##############################################
#verification des groupes
echo "====> Vérification des groupes"
id -nG|grep "docker"
if [ $? -ne 0 ];then
    echo "$USER n'appartient pas au groupe docker. Impossible de continuer"
    exit

fi
pause
##############################################
echo "====> Installation de kubernetes"
#chargement du repository
cat <<-EOF > ${TMP}/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
sudo cp ${TMP}/kubernetes.repo /etc/yum.repos.d/kubernetes.repo

sudo dnf install kubeadm -y 

sudo systemctl enable kubelet
sudo systemctl start kubelet


cat << EOF > ${TMP}/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF

sudo  cp ${TMP}/k8s.conf /etc/sysctl.d/k8s.conf

sudo sysctl --system

# suppression du swap
sudo swapoff -a

#suppression du swap dans /etc/fstab
sudo sed 's/\/dev\/mapper\/cl-swap/#\/dev\/mapper\/cl-swap/' /etc/fstab > ${TMP}/fstab
sudo cp ${TMP}/fstab /etc/fstab

pause

echo "====> Ajout du noeud au cluster"
#COMMANDE DE JOIN
# entrez ici la commande de join issue du master

exit

