#!/bin/bash
#####################################################
#  Installation de docker-ce
# NE RIEN MODIFIER CI-DESSOUS
#########################################################
USERHOME="/home/user"
TMP="${USERHOME}/tmp"
#########################################################
# fonctions
#########################################################
function pause(){
 echo ""
 read -s -n 1 -p "====> Presser une touche pour continuer . . ."
 echo ""
 echo ""
}


############################################################################################
###  PREPARATION MACHINE								 ###
############################################################################################
mkdir -p ${TMP}
sudo mkdir -p /etc/docker





#========================================================
#  service docker
#========================================================
#Installation des certificats pour le registre docker.licence.proj

sudo mkdir -p /etc/docker/certs.d/docker.licence.proj:5000
sudo cp /home/user/docker.licence.proj.crt /etc/docker/certs.d/docker.licence.proj:5000/
sudo chmod +r /etc/docker/certs.d/docker.licence.proj\:5000/docker.licence.proj.crt


# installation de docker et démarrage
sudo yum erase podman buildah

echo "=====> Installation et configuration de docker-ce"
sudo dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
sudo dnf install docker-ce --nobest -y

cat << 'EOF' > $TMP/daemon.json 
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2",
  "storage-opts": [
    "overlay2.override_kernel_check=true"
  ],  
  "registry-mirrors": ["https://mirror.gcr.io", "https://docker.licence.proj:5000"]
}
EOF
sudo cp $TMP/daemon.json /etc/docker/daemon.json


#Demarrage de docker
sudo systemctl start docker
sudo systemctl enable docker

pause
 
echo "=====> ajout de $USER au groupe docker"
sudo usermod -aG docker ${USER}
#su - ${USER}


echo "Vos groupes sont pour le moment :"
id -nG
pause
echo "Pour que la modification soit effective vous devez fermer la session"
echo "Vérifiez que user appartient bien au groupe docker avec la commande : id -nG"

exit
