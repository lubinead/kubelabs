#!/bin/bash
#########################################################
# Parametres à modifier
#########################################################
GW="10.1.X.254"
DNS="10.1.147.199"
DNSSEARCH="kubeY.rt.iut"

INTERFACE="enp0s3"
IPADDR="10.1.X.X"
MASK="24"
HOSTNAME="master-node.$DNSSEARCH"

MASTER_IPADDR="10.1.X.A"
WORKER1_IPADDR="10.1.X.B"
WORKER2_IPADDR="10.1.X.C"





#########################################################
# NE RIEN MODIFIER CI-DESSOUS
#########################################################
MASTER_NODENAME="master-node.$DNSSEARCH master-node"
WORKER1_NODENAME="node-1.$DNSSEARCH node-1"
WORKER2_NODENAME="node-2.$DNSSEARCH node-2"

USERHOME="/home/user"
TMP="/home/user/tmp"

#########################################################
# fonctions
#########################################################
function pause(){
 echo ""
 read -s -n 1 -p "====> Presser une touche pour continuer . . ."
 echo ""
 echo ""
}

function configure_ip {
sudo nmcli c down $INTERFACE
sudo nmcli c modify $INTERFACE ipv4.addresses ${IPADDR}/${MASK}
sudo nmcli c modify $INTERFACE ipv4.method static
sudo nmcli c modify $INTERFACE ipv4.gateway $GW
sudo nmcli c modify $INTERFACE ipv4.dns-search $DNSSEARCH
sudo nmcli c modify $INTERFACE ipv4.dns $DNS

sudo nmcli c modify $INTERFACE ipv6.method ignore
sudo nmcli c modify $INTERFACE ipv6.dns ""
sudo nmcli c modify $INTERFACE ipv6.dns-search ""
sudo nmcli c modify $INTERFACE ipv6.dns-options ""
sudo nmcli c modify $INTERFACE ipv6.dns-priority ""              
sudo nmcli c modify $INTERFACE ipv6.addresses ""                
sudo nmcli c modify $INTERFACE ipv6.gateway ""                
sudo nmcli c up $INTERFACE
}
############################################################################################
###  PREPARATION MACHINE								 ###
############################################################################################
mkdir -p ${TMP}

#========================================================
#preparation du réseau
#========================================================
#configuration du hostname
echo "=====> Configuration du hostname"
sudo hostnamectl set-hostname $HOSTNAME
pause

#desactivation du firewall
echo "=====> Desactivation du firewall"
sudo systemctl stop firewalld
sudo systemctl disable firewalld
pause

#décommenter pour configurer la stack ip
echo "=====> configuration de la pile IP"
configure_ip
pause

#configuration du fichier hosts
echo "=====> configuration du fichier hosts"
sudo rm -f /etc/hosts
sudo bash -c "cat << EOF > /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4 
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
$MASTER_IPADDR	$MASTER_NODENAME
$WORKER1_IPADDR	$WORKER1_NODENAME
$WORKER2_IPADDR	$WORKER2_NODENAME
EOF"

cat /etc/hosts

#configuration du serveur de temps
echo "====> Configuration du serveur de temps"
sudo sed 's/pool 2.centos.pool.ntp.org iburst/server ntp.rt.iut iburst/' /etc/chrony.conf > ${TMP}/chrony.conf
sudo cp ${TMP}/chrony.conf /etc/chrony.conf
sudo systemctl enable --now chronyd
pause

exit
