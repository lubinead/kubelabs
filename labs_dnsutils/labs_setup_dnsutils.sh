#!/bin/sh
kubectl apply -f ./dnsutils.yaml
kubectl get pods dnsutils

echo "Pour utiliser ce pod : lancer le script en mode interactif :"
echo "    kubectl exec -it dnsutils -- /bin/sh"

echo "Pour lancer une commande directement (par exemple: nslookup kubernetes.default :"
echo "    kubectl exec -it dnsutils --  nslookup kubernetes.default"

exit