#!/bin/sh

MYDIR=$(dirname "$0") 
DATE=$(date  "+%y%m%d%H%M%S")
cd $MYDIR

sudo mkdir -p /etc/docker/certs.d/docker.licence.proj:5000
sudo cp docker.licence.proj.crt /etc/docker/certs.d/docker.licence.proj:5000/
sudo chmod +r /etc/docker/certs.d/docker.licence.proj\:5000/docker.licence.proj.crt

#sauvegarde du fichier existant
sudo cp /etc/docker/daemon.json /etc/docker/daemon.json.save.$DATE

#remplacement
sudo cp daemon.json /etc/docker/daemon.json

