#!/bin/sh

search() {
    
    local i=0;
    
    for str in "${X[@]}"; do
        if [ "$str" = "$1" ]; then
            echo $i
            return $i
        else
            ((i++))
        fi
    done
    echo "-1"
}

sudo dnf install jq
cat <<'EOF'
===============================================================================================
Etude du parcours d'un paquet dans weave (entre podssur deux noeuds différents)
================================================================================================
EOF

echo "============================================="
echo "Selection des IP des pods sur le noeud node-master"
echo "============================================="
kubectl get pods -o wide -A | grep NAMESPACE
kubectl get pods -o wide -A | grep master

echo 
echo "=========>Entrez l'une des ces IP, ce sera la source : "
read IPSRC

NAMESPACESRC=$(kubectl get pods -o wide -A | grep master |grep $IPSRC |  tr -s ' ' | cut -d ' ' -f1)
PODSRC=$(kubectl get pods -o wide -A | grep master |grep $IPSRC |  tr -s ' ' | cut -d ' ' -f2)
echo
echo "Le pod source est $PODSRC : $IPSRC (dans l' espace de nom $NAMESPACESRC) "


echo "============================================="
echo "Selection des IP des pods sur le noeud node-1"
echo "============================================="
kubectl get pods -o wide -A | grep NAMESPACE
kubectl get pods -o wide -A | grep node-1

echo 
echo "=========>Entrez l'une des ces IP, ce sera la destination"
read IPDST

NAMESPACEDST=$(kubectl get pods -o wide -A | grep node-1 |grep $IPDST |  tr -s ' ' | cut -d ' ' -f1)
PODDST=$(kubectl get pods -o wide -A | grep node-1 |grep $IPDST |  tr -s ' ' | cut -d ' ' -f2)
echo
echo "Le pod destination est $PODDST : $IPDST (dans l' espace de nom $NAMESPACEDST) "
clear
echo "============================================="
echo "RESUME"
echo "============================================="

echo "==> Le pod source est $PODSRC : $IPSRC (dans l' espace de nom $NAMESPACESRC) "
echo "==> Le pod destination est $PODDST : $IPDST (dans l' espace de nom $NAMESPACEDST) "

echo
echo "============================================="
echo "Test de connexion"
echo "============================================="
kubectl exec -it $PODSRC -n $NAMESPACESRC -- ping -c2 $IPDST |grep "2 packets received" 
if [ $? -ne 0 ]; then
    echo "La destination est injoignable"
    exit -1
fi

echo
echo "============================================="
echo "La table de routage du POD SOURCE est:"
echo "( kubectl exec -it $PODSRC -n $NAMESPACESRC -- ip route )"
echo "============================================="
kubectl exec -it $PODSRC -n $NAMESPACESRC -- ip route
read -p "Continuer ? "
echo
echo "============================================="
echo "La route choisie sera :"
echo "(kubectl exec -it $PODSRC -n $NAMESPACESRC -- ip route get $IPDST )"
echo "============================================="

kubectl exec -it $PODSRC -n $NAMESPACESRC -- ip route get $IPDST
read -p "Continuer ? "

# La route est elle non directement connectée ? Elle contient le mot clé via
kubectl exec -it $PODSRC -n $NAMESPACESRC -- ip route get $IPDST |grep via

if [ $? -ne 0 ]
then # le mot clé via n'a pas été trouvé
    echo
    echo "============================================="
    echo "Etat de la table arp pour la destination sélectionnée :"
    echo "( kubectl exec -it $PODSRC -n $NAMESPACESRC -- ip neigh |grep $IPDST )"
    echo "============================================="
    kubectl exec -it $PODSRC -n $NAMESPACESRC -- ip neigh |grep $IPDST 
    MACDST="$(kubectl exec -it $PODSRC -n $NAMESPACESRC -- ip neigh |grep $IPDST |tr -s ' ' | cut -d ' ' -f5 )"
    read -p "Continuer ? "
    echo
    echo "============================================="
    echo "la table forward du bridge virtuel indique :"
    echo "============================================="
    bridge fdb |grep $MACDST 
    read -p "Continuer ? "

    echo
    echo "============================================="
    echo "L'interface de sortie du bridge est :"
    echo "============================================="

    OUTIF2="$(bridge fdb |grep $MACDST|tr -s ' ' | cut -d ' ' -f3)"
    echo $OUTIF2
    read -p "Continuer ? "
    echo
    echo "============================================="
    echo "The packet is sent from the weave bridge down to the OVS kernel datapath over a veth link:"
    echo "============================================="
    echo

    ip link | grep $OUTIF2
    read -p "Continuer ? "
    echo
    echo "============================================="
    echo " Selection du flux de sortie dans weave pour cette adresse MAC destination"
    echo " (Analyse de la sortie de weave report)"
    echo " La ligne action indique le port virtuel à utiliser" 
    echo "============================================="

    weave report > tempo.json
    #cat tempo.json|grep "EthernetFlowKey{src: "|tr -s ' ' |cut -d ' ' -f 3,5 |tr ',' ' '

    X=( $(cat tempo.json|grep "EthernetFlowKey{src: "|tr -s ' ' |tr '}' ' '|cut -d ' ' -f 5 |tr ',' ' ' ) )

    INDEX="$(search $MACDST)" 
    cat tempo.json |jq .| jq .Router.OverlayDiagnostics.fastdp.Flows[$INDEX]
    VPORT=$( cat tempo.json |jq .| jq .Router.OverlayDiagnostics.fastdp.Flows[$INDEX] |grep OutputAction|tr '{}' ' '|tr -s ' '|cut -d ' ' -f 4 |uniq)
    read -p "Continuer ? "

    echo
    echo "============================================="
    echo "le port de sortie requis par le flux est :"
    echo "============================================="
    echo "VPORT=$VPORT"
    read -p "Continuer ? "
    echo
    echo "============================================="
    echo "Configuration du vport de sortie :"
    echo "============================================="
    cat tempo.json | jq . | jq .Router.OverlayDiagnostics.fastdp.Vports[$VPORT]
    read -p "Fin de l'analyse ? "
    #ss -lnpu
    exit 0

else
    echo " Le paquet est routé et traduit (SNAT vers l'extérieur du réseau WEAVE)"
    echo



    IPGW=" $( kubectl exec -it $PODSRC -n $NAMESPACESRC -- ip route get $IPDST |grep via |cut -d ' ' -f3 )"
    echo "============================================="
    echo "Etat de la table arp pour la passerelle vers l'extérieur :"
    echo "( kubectl exec -it $PODSRC -n $NAMESPACESRC -- ip neigh |grep $IPGW )"
    echo "============================================="
    kubectl exec -it $PODSRC -n $NAMESPACESRC -- ip neigh |grep $IPGW
    MACDST="$(kubectl exec -it $PODSRC -n $NAMESPACESRC -- ip neigh |grep $IPGW |tr -s ' ' | cut -d ' ' -f5 )"
    read -p "Continuer ? "
fi


