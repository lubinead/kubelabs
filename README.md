# KUBELABS
22/12/2021
Ce jeux de fichiers permet de réaliser un TP sur les bases de  Kubernetes
Il est concu pour un cluster sous centos8
- 1 maitre 
- 2 noeuds worker

Les scripts sont conçus pour une install en mode user (et pas root !)

**Labs-init-réseau.sh** permet de configurer le réseau mais il n'est adapté qu'a une configuration locale.
On peut tout à fait s'en passer à condition de prendre soin de configurer proprement sur les trois noeuds.
- Les IP
- Les noms de machine
- Un DNS
- un fichier hosts contenant les références des trois noeuds 

**labs_install_docker** remplace podman par docker_ce.
Attention à la personalisation de /etc/docker/daemon.json 
Il faut faire pointer le registry mirror sur un registre approprié (a défaut celui de docker.io directement)

**labs_jubernetes_install_master.sh** installe kubernetes sur le noeud maitre.
Prendre soin de debugguer les erreurs détectées lors de la préparation "preflight errors"
Testé sur centos8.4 mais semble fonctionne aussi sur proxmox/LXC (avec le conteneur centos intégré + updates)
le CNI Weave est installé ainsi que l'applicatif Weave-Scope



Infos diverses
<details><summary>Installation de docker</summary>
- La clé associée à mon registre personnel n'est pas fournie ici.
- Le script utilise docker-ce et desinstalle podman, lequel est en fait absent sur les docker LXC.

</details>


<details><summary>Installation de Kubernetes sur LXC (proxmox)</summary>
- il manque le fichier /etc/kmsg dans LXC. Il est possible de le définir sous forme de lien dans /etc/rc.d/rc.local

[ -e /dev/kmsg ] || ln -s /dev/console /dev/kmsg

Puis chmod +x /etc/rc.d/rc.local

</details>


<details><summary>Bugs liés aux mises à jour</summary>
- version 1.23.1
metrics-server ne demarrer qu'en rajoutant une clause --kubelet-insecure-tls

```
- args:
        - --cert-dir=/tmp
        - --secure-port=4443
        - --kubelet-insecure-tls
        - --kubelet-preferred-address-types=InternalIP,ExternalIP,Hostname
        - --kubelet-use-node-status-port
        - --metric-resolution=15s

```
</details>


<details><summary>A propos de la swap</summary>
Les versions récentes de Kubernetes acceptent une swap activée sur les noeuds. L'installation se fait avec swap desactivée (sur proxmox/LXC faire un swapoff également sur l'hyperviseur).
Il fut ensuite intervenir sur /var/lib/kubelet/config pour réactiver la gestion de la swap.

</details>






