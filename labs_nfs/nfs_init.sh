#!/bin/sh

mkdir -p /data

mkdir -p /home/user/tmp
TMP="/home/user/tmp"

cat << EOF > ${TMP}/exports
/data           10.1.X.0/24(rw,sync,no_root_squash)
EOF

sudo cp ${TMP}/exports /etc/exports

sudo dnf install nfs-utils

sudo systemctl enable --now nfs-server

sudo exportfs -v
sudo exportfs -s

