#!/bin/bash 

IP_CLUSTER=X.X.X.
 
for i in {1..200} ; do
   curl $IP_CLUSTER 2>/dev/null > output.txt
   grep "node name" output.txt | tee -a req_nodes.txt
   grep "pod IP"  output.txt | tee -a req_pods.txt
done; # trier et compter le nombre de requêtes par serveur 

sort req_nodes.txt | uniq -c 
sort req_pods.txt | uniq -c 

rm output.txt req_nodes.txt req_pods.txt
