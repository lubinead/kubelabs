#!/bin/sh
##################################################
### NE RIEN MODIFIER																
###################################################

USERHOME="/home/user"
TMP="/home/user/tmp"

mkdir -p ${TMP}


function pause(){
 echo ""
 read -s -n 1 -p "====> Presser une touche pour continuer . . ."
 echo ""
 echo ""
}

##############################################
### Installation de Kubernetes
##############################################
#verification des groupes
echo "====> Vérification des groupes"
id -nG|grep "docker"
if [ $? -ne 0 ];then
    echo "$USER n'appartient pas au groupe docker. Impossible de continuer"
    exit

fi
pause

##################################
echo "====> Creation du repository"
cat <<-EOF > ${TMP}/kubernetes.repo
	[kubernetes]
	name=Kubernetes
	baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
	enabled=1
	gpgcheck=1
	repo_gpgcheck=1
	gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

sudo cp ${TMP}/kubernetes.repo /etc/yum.repos.d/kubernetes.repo
pause




echo "====> Installation de kubernetes"
sudo dnf install kubeadm -y 

sudo systemctl enable kubelet
sudo systemctl start kubelet


cat << EOF > ${TMP}/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF

sudo  cp ${TMP}/k8s.conf /etc/sysctl.d/k8s.conf

sudo sysctl --system

#desactivation du swap
sudo swapoff -a
sudo sed 's/\/dev\/mapper\/cl-swap/#\/dev\/mapper\/cl-swap/' /etc/fstab > ${TMP}/fstab
sudo cp ${TMP}/fstab /etc/fstab
pause


echo "====> Initialisation avec kubeadm init"
sudo kubeadm init
pause


echo "====> Configuration de $HOME/.kube/config "
#enable user to start the cluster
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

#completion automatique
echo "source <(kubectl completion bash)" >> ~/.bashrc
pause




echo "====> verification de la présence d\'un noeud maitre"
kubectl get nodes
pause


echo "====> création du réseau dédié aux pods -  Installation de weave"
# remarque : une version locale du fichier yaml peut être téléchargée par
# curl -fsSLo weave-daemonset.yaml "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
# et pourrait être appliquée le cas échéant à la place de l'appel en ligne direct.
# cela permettrait en particulier de modifier le réseau par defaut
# réseau par defaut 10.32.0.0/12
# voir : https://www.weave.works/docs/net/latest/kubernetes/kube-addon/#troubleshooting

export kubever=$(kubectl version | base64 | tr -d '\n')
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$kubever"

#installation de l'outil weave en ligne de commande 
sudo curl -L git.io/weave -o /usr/local/bin/weave
sudo chmod a+x /usr/local/bin/weave
pause

echo "===> Installation de weave-scope"
# creation d'un weave-scope
kubectl apply -f 'https://cloud.weave.works/k8s/scope.yaml'
exit
