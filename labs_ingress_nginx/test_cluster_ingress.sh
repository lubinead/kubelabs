#!/bin/bash 

TARGET_HOST=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
 
for i in {1..200} ; do
   curl -i $TARGET_HOST  2>/dev/null > output.txt
   grep "pod IP" output.txt |cut -d " " -f 2| tee -a req_pods.txt
done; # trier et compter le nombre de requêtes par serveur 

sort req_pods.txt | uniq -c 


rm output.txt req_pods.txt
