#!/bin/bash
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx

helm repo update

kubectl create namespace ingress-nginx

helm install -n ingress-nginx ingress-nginx-ctl ingress-nginx/ingress-nginx
