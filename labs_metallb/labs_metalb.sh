#!/bin/bash
#====================================================
# installateur par manifeste
#====================================================
echo "====> Téléchargement du fichier namespace.yaml"
#wget  https://raw.githubusercontent.com/metallb/metallb/v0.9.5/manifests/namespace.yaml
echo "====> Téléchargement du fichier metallb.yaml"
#wget  https://raw.githubusercontent.com/metallb/metallb/v0.9.5/manifests/metallb.yaml
echo "====> application des fichiers"
kubectl apply -f namespace.yaml
kubectl apply -f metallb.yaml

# On first install only
echo "====> création d\'un secret partagé"
kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)"


echo "ATTENTION !! le systeme restera inopérationnel jusqu\'à création d\'une config map"



cat <<'EOF'

Layer 2 mode is the simplest to configure: in many cases, you don’t need any protocol-specific configuration, only IP addresses.

Layer 2 mode does not require the IPs to be bound to the network interfaces of your worker nodes. It works by responding to ARP requests on your local network directly, to give the machine’s MAC address to clients.

For example, the following configuration gives MetalLB control over IPs from 10.1.X.240 to 10.1.X.249, and configures Layer 2 mode:
You should buidld and apply such a config map

---
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: metallb-system
  name: config
data:
  config: |
    address-pools:
    - name: default
      protocol: layer2
      addresses:
      - 10.1.X.240-10.1.X.249

EOF